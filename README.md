# Laconicraft

## The Summary

This data pack for Minecraft: Java Edition (and behavior pack for Bedrock) removes some redundant mobs and items while rebalancing others. The goal is to cut out some of the bloat that the game has accumulated over time, improve inventory management, and encourage exploration. Some notable changes include:

- Pigs are now the only land animal to drop meat.
- Removed naturally-spawning cows in favor of goats for milk and rabbits, llamas, and horses for leather.
- Removed potatoes and beetroots.
- Merged cod and salmon meat.

Available for Java Edition 1.20.3 through 1.20.6 (specifically 23w41a through 1.21 Pre-Release 2) and Bedrock 1.20.10+.

## The Story

Long ago, in the age of Beta 1.7, every mob had purpose, and every food had a niche and unique farming mechanics. But over the past decade, despite many great additions, Minecraft has nevertheless been bloated with several redundant features that not only add little of note, but also undermine existing content. There are now 6 land animals that drop meat, but unlike their competition, pigs have gained no additional features to make them stand out from the rest. Potatoes and beetroots have added almost nothing to the game, at the cost of competing for inventory space with similar food items like wheat and carrots.

Some mods attempt to solve this by adding a bunch of new features to the lacking content. In some cases, that's the best approach. But this pack takes a simpler approach: just cut the amount of similar content down to the most useful and/or interesting. And to get there, I won't spare a feature just because it's "classic". In short, I'm making Minecraft more _laconic_.

For example, rabbits and goats are behaviorally more interesting than cows, yet both are rarely used since cows are so common. The solution? Make cows way rarer (thus making goats useful for milk and rabbits useful for leather). And while we're at it, let's make leather armor more viable by increasing leather drops from mobs like llamas. And once the relatively expensive-to-breed llamas drop a lot of leather, we can remove rabbit hide and make the cheaper rabbits drop a single piece of leather each. Just like that, we've addressed 3 issues:

- Irrelevancy of rabbits and goats (and to a lesser extent llamas and other beasts of burden).
- Leather armor being generally too expensive compared to the comparative ease of obtaining iron.
- Pointlessness of rabbit hide.

## The Details

### Changes in both versions

#### More leather (and no more rabbit hide)

When was the last time you used leather armor? It's rather expensive, considering its low durability and the relative ease of obtaining iron armor. And what's the point of rabbit hide: a single-use item that functions as nothing more than a smaller, less useful piece of leather? This pack addresses both problems.

Rabbit hide has been removed from the cat gift and rabbit loot tables and replaced with leather. Rabbits now always drop 1 leather (or more with looting). If you want to craft bundles, I recommend using the [leather recipe data pack from Vanilla Tweaks](https://vanillatweaks.net/picker/crafting-tweaks/).

Horse/donkey/mule leather drop is raised from 0-2 to 3-4. Llama leather drop is raised from 0-2 to 2-3. Mooshroom/cow leather drop is raised from 0-2 to 1-2.

To account for the increase in leather and avoid over-cheapening the cost of enchanted books, the crafting recipe for books now requires 2 leather instead of 1.

On Bedrock, I've removed rabbit hide from the list of items that foxes can spawn holding in their mouth. (They already had a chance to spawn holding leather, and I didn't think it was necessary to increase the odds of getting leather.) On Java Edition, where these spawn items are currently hardcoded, I've instead taken the approach of periodically converting any rabbit hide in the mouth of a fox to the result of a custom loot table based on the Bedrock one.

#### Very rare cows

There are some mobs too old and iconic for Mojang to remove... but I'm not Mojang. This pack removes naturally-spawned cows, since other mobs with more unique behavior can provide the same items while encouraging more interesting gameplay. To elaborate:

- Goats provide milk, while also having the unique ramming and jumping mechanics. The latter makes fencing them in more challenging, which should encourage creative building. Since they only spawn on mountains, players must brave the cliffs (and risk falling off a ledge or into powder snow) before they can obtain milk (a useful tool when combating poison and mining fatigue). They also sound funnier.
- Horses drop leather and you can ride them.
- Llamas drop leather and can be used to form a mobile storage train. They also attack wolves, which is more interesting than cows which neither attack nor are attacked by anything.
- Rabbits drop leather, look cuter, are more of a challenge to trap/kill, and have more interaction with the rest of the world: they eat your crops and are preyed upon by foxes, encouraging you to build stuff to keep them safe (and away from your crops).
- Horses, llamas, and rabbits all spawn in fewer biomes than cows, encouraging exploration.

Notably, mooshrooms remain in the game, serving as a rare, high-tier mob that provides both leather and milk, as well as mushroom stew. They're too rare to interfere with the usefulness of the other mobs in most gameplay, while also increasing the appeal of the mushroom fields biome.

You can shear a mooshroom to create a cow, though there's no practical reason to do this unless you really need mushrooms for something. Cows are now treated as a variant of the mooshroom for the purpose of advancements, so you only need to breed either mooshrooms or cows for "Two by Two", not both.

Technical note: In Java Edition, most cows are sent to the void whenever they're loaded; however, cows made by shearing mooshrooms or by breeding are tagged to avoid sending them to the shadow realm; name-tagged cows are also preserved. In Bedrock, no new cows spawn naturally, but all existing cows are preserved.

#### Porkchop master race

Removed meat drops from chickens, mooshrooms, rabbits and sheep. Pigs are now the only passive, non-aquatic mob that drops food, thus making them useful again.

To prevent the other passive mobs from sometimes dropping nothing, their loot tables have been adjusted:

Raised minimum feather drop count for chickens from 0 to 1.
Raised minimum leather drop count for mooshrooms/cows from 0 to 1.
Rabbits, as mentioned before, now always drop 1 leather (looting aside) instead of 0-1 rabbit hide.

Chest loot tables have been altered such that all entries for the "removed" meats have been merged/replaced with porkchops. The meat versus non-meat item ratios have been kept intact.

Butcher villager trades have been adjusted to account for the lack of other meats to buy/sell. All the non-porkchop meat trades are gone (as well as trades involving rabbit stew), and the buy-porkchops trade has been moved from apprentice to journeyman level (because otherwise there wouldn't be anything in that level).

Instead of cats gifting raw chicken to players, they will now gift raw cod. (Changing it to porkchops wouldn't make sense since cats don't hunt pigs.)

For storage convenience, crafting recipes have been added to allow you to convert your existing beef/mutton/etc. into porkchops.

#### Removed salmon and tropical fish meats

Tropical fish meat has been removed and replaced with pufferfish in all loot tables. Thus, tropical fish no longer drop any meat. This has no effect on axolotls, since they eat "live" (AKA bucketed) tropical fish.

Salmon meat has been replaced with cod meat in all loot tables. An optional resource pack is provided to rename the items from "cod" to "fish". Salmon mobs still exist but drop the same meat as cod.

The salmon-related fisherman villager trades have been removed, and the cod-for-emeralds trade has been moved from apprentice to expert level. Since the tropical fish trade is gone (due to tropical fish meat being removed), this ensures that there is still a trade for the expert level. It also nerfs the trade (which would otherwise be a lot easier to use than vanilla since both salmon and cod now drop the same item).

I chose cod meat over salmon because it's less filling. Fish spawn continually, with no need to conserve them like pigs, so it's best if fish remain a significantly weaker food source to counter their abundance.

Additionally, the reduced types of fish meat items should make inventory management a bit less annoying.

#### Beetroots be gone, potatoes go poof

Beetroots, potatoes, and carrots are all pretty similar, and none are particularly useful. All can be used to breed pigs, and all are rather low-tier foods. I think only one needs to exist.

I chose to keep carrots, because they have golden carrots and night vision potions to justify their existence.

Beetroots and potatoes have been removed from all loot tables and generation. Specifically:

- Replaced potatoes and beetroots in various chests with carrots. The ratio of vegetables to other loot was kept more-or-less the same.
- Replace beetroot soup in village chests with mushroom stew, which happens to be functionally identical.
- Removed beetroot seeds from mineshaft, dungeon, spawn bonus, and woodland mansion chests; increased pumpkin/melon seeds to compensate.
- Replaced beetroot seeds with wheat seeds in village snowy house chests.
- Replaced beetroot seeds in end city chests with pumpkin seeds, because pumpkins are useful when dealing with endermen.
- Replaced beetroot seeds in trail ruins with pumpkin seeds.
- Replaced baked potatoes in ancient city ice box chests with carrots (since golden carrots are also found in ancient cities). To compensate for carrots refilling less hunger/saturation, I doubled their max count to compensate.
- Replaced baked potatoes in trail chamber chests with bread, which restores the same amount of hunger/saturation.

Farmer villager trades involving beetroots, beetroot seeds, and potatoes have been removed.

In Java Edition, the removed crops no longer generate in village farms. I accomplished this via Java Edition's `processor_list` files. In village variants that already generated carrots, the ratio of wheat to non-wheat crops has been preserved.

Unable to find a Bedrock equivalent, I am currently using a hackier (but "good enough") solution: every 20 ticks (1 second), all beetroot and potato crops in a 20x20x20 cube around the player are replaced with carrots, preserving the growth stage in the process.

#### Charcoal cast into the fire

Smelting logs now produces coal. You might say this is technically unrealistic, but so is getting "bone meal" from a composter that you only threw a bunch of crops into.

Notably, this makes coal blocks renewable on peaceful difficulty.

### Java-only changes

The "Hero of the Village" butcher gift loot table has been adjusted to remove non-pork meats. (Bedrock doesn't have the gift mechanic yet, so there's nothing to change there.)

The removed or now-inaccessible mobs and items are no longer required for any advancements.

The data pack will alter the trades of novice villagers, but not those that have already leveled up. (I'm actually not sure what the behavior is for the Bedrock pack. I assume that pre-existing villagers are not changed.)

## Things I would've changed, but can't

Feel free to "steal" these ideas and implement them yourself in a mod. (And send me the link so I can try it out!)

I would have removed sheep and given the wool drops and dye/shears functionality to goats, but there's no practical way to replicate the dying/shearing mechanics using only a data pack. Since goats only spawn on mountains, this would limit access to wool (and thus beds), requiring players to put in some effort before they gain the ability to skip the horrors of the night… unless they spawn near a village.

I'd give parrots the functionality of the allay (and remove the latter once its added), but this is definitely mod-only territory, and certainly a lot trickier to implement than the goat idea above.

## Things I could've changed, but didn't

I considered removing pigs and making another land mob drop meat. Since hoglins drop porkchops, I probably would've renamed porkchops to just "meat", and made that the drop of whichever Overworld land mob I chose. For now, I've decided against it, since pigs are significant to the lore of Minecraft because of the Nether. I may reconsider later.

## Credits

The villager trade modification code in the Java Edition data pack was originally derived from [this pack](https://www.planetminecraft.com/data-pack/custom-villager-trades-customize-your-villagers-modders-tool/) by BlockyGoddess.

The axe in the pack icon is the [Dungeons axe](https://minecraft.wiki/w/Minecraft_Dungeons:Axe).

Most files in this pack are based on the vanilla ones, so credit to Mojang and everyone else who worked on Minecraft, of course.

Finally, I thank God for giving me the creativity and time to make this project.

## Recommended packs/mods by other people

Want to further improve Minecraft? Here's what I recommend…

### Resource/data packs

- [Glowier Berries](https://modrinth.com/datapack/glowier-berries) - Make yourself glow by eating glow berries! Great for finding your friends in caves!
- [Vanilla Tweaks](https://vanillatweaks.net/) and [Bedrock Tweaks](https://bedrocktweaks.net/) - Tons of resource packs, data packs, and behavior packs to improve and customize Minecraft in simple but effective ways.

### Mods

#### Both Fabric and Forge

- [Apple Skin](https://modrinth.com/mod/appleskin) - No more guessing about saturation and food stats!
- [Augmented Autojump](https://modrinth.com/mod/augmented-autojump) - Auto-jump, but you might actually want to use it!
- [Damage Tilt (for versions before 1.19.4)](https://www.curseforge.com/minecraft/mc-mods/damage-tilt) - Brings back an old feature from 1.2 that changes the on-damage screen tilt depending on where the damage came from.
- [Mouse Tweaks](https://modrinth.com/mod/mouse-tweaks) - Faster inventory management via new mouse dragging behavior.
- [Not Enough Animations](https://modrinth.com/mod/not-enough-animations) - More player animations!
- [Noteblock Expansion](https://www.curseforge.com/minecraft/mc-mods/noteblock-expansion) - Makes several blocks more useful by giving them unique note block sounds!

#### Fabric-only

- [Anvil Crushing Recipes](https://modrinth.com/mod/anvil-crushing-recipes) - A fun new block conversion mechanic using anvils!
- [Carpet](https://modrinth.com/mod/carpet) - Tons of configurable tweaks/additions to vanilla mechanics, including movable block entities!

#### Forge-only

- [Carry On](https://modrinth.com/mod/carry-on) - Pick up chests and stack animals!
- [Glazed Symmetry](https://www.curseforge.com/minecraft/mc-mods/glazed-symmetry) - Center-tile glazed terracotta blocks to make odd-length patterns!
- [Herd Mentality](https://modrinth.com/mod/herd-mentality) - Add some extra challenge to your animal hunting!
- [Mindful Eating](https://www.curseforge.com/minecraft/mc-mods/mindful-eating) - Makes different food types more effective for different activities.
- [Nyf's Spiders](https://modrinth.com/mod/nyfs-spiders) - Way cooler spiders.
- [Quark](https://quarkmod.net/) - Perhaps the greatest little-additions mod ever. Movable block entities, dispensers placing blocks, inventory sorting, and far too many other configurable changes and additions to list! Watch out for the troon bees and other degeneracy shout-outs, though.
- [Snow! Real Magic!](https://modrinth.com/mod/snow-real-magic) - Snowiest snow!
- [Trample No More](https://modrinth.com/mod/trample-no-more) - Leather boots or feather falling stop crop trampling.

## The (Un)licensing

Since a lot of the stuff here is based on vanilla files, the copyright status of a lot of it is a bit unclear. I doubt most of it is copyrightable to begin with. But at least with regard to all of my own contributions, they are free and unencumbered software released into the public domain. See UNLICENSE.txt for more details.
