data remove entity @s Offers.Recipes[-1]
data remove entity @s Offers.Recipes[-1]

data modify entity @s Offers.Recipes append value {buy:{id:"minecraft:pumpkin",Count:6},sell:{id:"minecraft:emerald",Count:1},xp:10,rewardExp:1b,priceMultiplier:0.05f,maxUses:12}

execute store result score @s laconicraft_villager_rng run random value 1..2 laconicraft:trades/villager

data modify entity @s[scores={laconicraft_villager_rng=1}] Offers.Recipes append value {buy:{id:"minecraft:emerald",Count:1},sell:{id:"minecraft:pumpkin_pie",Count:4},xp:5,rewardExp:1b,priceMultiplier:0.05f,maxUses:12}

data modify entity @s[scores={laconicraft_villager_rng=2}] Offers.Recipes append value {buy:{id:"minecraft:emerald",Count:1},sell:{id:"minecraft:apple",Count:4},xp:5,rewardExp:1b,priceMultiplier:0.05f,maxUses:16}

scoreboard players reset @s laconicraft_villager_rng

return 1
