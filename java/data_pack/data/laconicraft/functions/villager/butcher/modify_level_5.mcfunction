# This profession level only has 1 trade in vanilla.
data remove entity @s Offers.Recipes[-1]

data modify entity @s Offers.Recipes append value {buy:{id:"minecraft:sweet_berries",Count:10},sell:{id:"minecraft:emerald",Count:1},xp:30,rewardExp:1b,priceMultiplier:0.05f,maxUses:12}

return 1
