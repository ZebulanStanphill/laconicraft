# An unemployed villager has no trades, and so all tags indicating that they were updated should be removed.
tag @s remove laconicraft_villager_updated_1
tag @s remove laconicraft_villager_updated_2
tag @s remove laconicraft_villager_updated_3
tag @s remove laconicraft_villager_updated_4
tag @s remove laconicraft_villager_updated_5
