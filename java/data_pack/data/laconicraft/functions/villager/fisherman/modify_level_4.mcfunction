# This profession level only has 1 trade in vanilla.
# Remove tropical-fish-for-emeralds trade.
data remove entity @s Offers.Recipes[-1]

# Add cod-for-emeralds trade.
data modify entity @s Offers.Recipes append value {buy:{id:"minecraft:cod",Count:15},sell:{id:"minecraft:emerald",Count:16},xp:10,rewardExp:1b,priceMultiplier:0.05f,maxUses:16}

return 1
