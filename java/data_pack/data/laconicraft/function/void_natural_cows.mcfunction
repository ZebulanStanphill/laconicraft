# Tag name-tagged cows so they won't be voided.
execute as @e[type=cow] if data entity @s CustomName run tag @s add laconicraft_keep

# Tag baby cows bred from tagged cows so that they won't be voided.
execute as @e[type=cow,tag=laconicraft_keep] at @s run tag @e[type=cow,predicate=laconicraft:is_baby_mob,distance=..1] add laconicraft_keep

# Tag cows made by shearing mooshrooms via a dispenser. (We can't catch dispenser-based shearing via advancements like we can player-based shearing.)
execute as @e[type=cow,tag=!laconicraft_keep] at @s if entity @e[predicate=laconicraft:is_sheared_mooshroom_item,distance=..2] run tag @s add laconicraft_keep

# Send all non-tagged cows to the void.
tp @e[type=cow,tag=!laconicraft_keep] ~ ~-10000 ~
