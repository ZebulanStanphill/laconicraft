$tag @s add laconicraft_villager_updated_$(level)

# Future-proof villagers so that, if this data pack ever gets updated with new/different trades, we'll know which villagers need to be updated again.
tag @s add laconicraft_v1
