# Remove all trades from this level. In vanilla there are 3 possible trades, 2 of which get picked. We don't know if the 2nd one is the cook-salmon trade or the campfire trade, and we need to move the cod-for-emeralds trade anyway, so we just remove all trades.
data remove entity @s Offers.Recipes[-1]
data remove entity @s Offers.Recipes[-1]

# Then we add back the campfire trade.
data modify entity @s Offers.Recipes append value {buy:{id:"minecraft:emerald",count:2},sell:{id:"minecraft:campfire",count:1},xp:5,rewardExp:1b,priceMultiplier:0.05f,maxUses:12}

return 1
