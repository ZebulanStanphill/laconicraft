# In vanilla, there is a pool of potential trades for each villager profession level. If there are 2 or less, the whole pool will be added; otherwise, 2 will be randomly selected. We know some trades in this profession level involve items that this pack makes unobtainable, so we must remove the last trades in the list (which correspond to the current level of the villager), and then we restore the trades that still make sense for this data pack.
data remove entity @s Offers.Recipes[-1]
data remove entity @s Offers.Recipes[-1]

data modify entity @s Offers.Recipes append value {buy:{id:"minecraft:porkchop",count:7},sell:{id:"minecraft:emerald",count:1},xp:2,rewardExp:1b,priceMultiplier:0.05f,maxUses:16}

return 1
