data remove entity @s Offers.Recipes[-1]
data remove entity @s Offers.Recipes[-1]

execute store result score @s laconicraft_villager_rng run random value 1..2 laconicraft:trades/villager

data modify entity @s[scores={laconicraft_villager_rng=1}] Offers.Recipes append value {buy:{id:"minecraft:wheat",count:20},sell:{id:"minecraft:emerald",count:1},xp:2,rewardExp:1b,priceMultiplier:0.05f,maxUses:16}

data modify entity @s[scores={laconicraft_villager_rng=2}] Offers.Recipes append value {buy:{id:"minecraft:carrot",count:22},sell:{id:"minecraft:emerald",count:1},xp:2,rewardExp:1b,priceMultiplier:0.05f,maxUses:16}

data modify entity @s Offers.Recipes append value {buy:{id:"minecraft:emerald",count:1},sell:{id:"minecraft:bread",count:6},xp:1,rewardExp:1b,priceMultiplier:0.05f,maxUses:16}

scoreboard players reset @s laconicraft_villager_rng

return 1
